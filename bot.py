#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import china_idiom as idiom
import telegram as tg
from telegram.ext import Updater
from telegram.ext import MessageHandler, Filters
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.DEBUG);

TOKEN = os.environ.get('tgToken')
updater = Updater(token=TOKEN);
dispatcher = updater.dispatcher;

def next(update, context):
	result = idiom.next_idioms_solitaire(update.message.text, count=3, heteronym=False)
	try:
		context.bot.send_message(chat_id=update.effective_chat.id, text=result[0]+'\n'+result[1]+'\n'+result[2])
	except IndexError:
		context.bot.send_message(chat_id=update.effective_chat.id, text='no such idiom')

next_handler = MessageHandler(Filters.text & (~Filters.command), next)
dispatcher.add_handler(next_handler)

updater.start_polling()

